package edu.towson.cis.cosc442.project3.vendingmachine;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class VendingMachineItemTest {
	
	VendingMachineItem VMItem1, VMItem2, VMItem3, VMItem4, VMItem5;

	@Before
	public void setUp() throws Exception {
		VMItem1 = new VendingMachineItem("Yummy Yums", 1.25);
		VMItem2 = new VendingMachineItem("", 1);
		VMItem3 = new VendingMachineItem("Free bees", 0);
		VMItem4 = new VendingMachineItem(null, 1.25);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test(expected = VendingMachineException.class) 		//Checks to make sure the vending machine throws
	public void testVendingMachineItem() {					// a vending machine exception if the price is 
		VMItem1 = new VendingMachineItem("We pay you", -1); // a negative number
		
	}

	@Test
	public void testGetName() { //Test a basic name to make sure the class getName() works
		assertEquals("Yummy Yums", VMItem1.getName());
	}
	
	@Test
	public void testGetEmptyName() { //Test to see if the getName() class works with a blank input
		assertEquals("", VMItem2.getName());
	}
	
	@Test(expected = NullPointerException.class) // Test to see if the getName() class throws a 
	public void testGetNullName() {				 // NullPointerException if the name is null
		assertEquals("", VMItem4.getName());
	}
	

	@Test
	public void testGetEmptyPrice() {	//Test to see if getPrice can take a zero value
		assertEquals(0, VMItem3.getPrice(), 0.001);
	}
	
	@Test
	public void testGetPrice() {		//Checks to see if the getPrice() class returns the correct price
		assertEquals(1.25, VMItem1.getPrice(), 0.001);
	}
	
	@Test(expected = VendingMachineException.class)
	public void testGetError() {		//Expects error
		VMItem5 = new VendingMachineItem("Less", -0.01);
		assertEquals(-0.01, VMItem5.getPrice(), 0.001);
	}
	

}
